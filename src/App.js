import React from "react";
//import logo from "./logo.svg";
//import "./App.css";

class Car extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: "Ford",
      model: "Mustang",
      color: this.props.color,
      year: 1964,
    };
  }

  // static getDerivedStateFromProps(props, state) {
  //   return { color: props.color };
  // }

  // componentDidMount() {
  //   setTimeout(() => {
  //     this.setState({ color: "yellow" });
  //   }, 1000);
  // }

  changeColor = () => {
    this.setState({ color: "blue" });
  };
  render() {
    return (
      <div>
        <h1>My {this.state.brand}</h1>
        <p>
          It is a {this.state.color} ;{this.state.model}
          <br />
          from {this.state.year}.
        </p>
        <button type="button" onClick={this.changeColor}>
          Change color
        </button>
      </div>
    );
  }
}

class Garage extends React.Component {
  render() {
    const carinfo = { name: "Ford", model: "Mustang" };
    return (
      <div>
        <h1>Who lives in my garage?</h1>
        <Car brand={carinfo} color="black" />
      </div>
    );
  }
}
export { Garage, Car };
